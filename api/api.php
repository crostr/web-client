<?php

    $method = $_GET['method'];
    if ($method === 'start') {
	    $stream_name = $_GET['stream_name'];
	    $stream_user = $_GET['stream_user'];
	    addStream($stream_name, $stream_user);
    }

    if ($method === 'stop') {
	    $stream_name = $_GET['stream_name'];
	    removeStream($stream_name);
    }

    if ($method === 'setcurrentstream') {
	$stream_name = $_GET['stream_name'];
	setCurrentStream($stream_name);
    }

    function setCurrentStream($stream_name) {
	$jsondata = json_encode(array('current_stream' => $stream_name) , JSON_PRETTY_PRINT);
	file_put_contents("current_stream.json", $jsondata);
    }

    function removeStream($stream_name) {
	$streams = file_get_contents("streams.json");
	$json = json_decode($streams, true);
	unset($json[$stream_name]);
	if (count($json) > 0) {
		$jsondata = json_encode($json , JSON_PRETTY_PRINT);
		file_put_contents("streams.json", $jsondata);
	} else {
		file_put_contents("streams.json", "{}");
	}
    }

    function addStream ($stream_name, $stream_user) {
	$streams = file_get_contents("streams.json");
	$json = json_decode($streams, true);
	$json[$stream_name] = array('user' => $stream_user);
	$jsondata = json_encode($json , JSON_PRETTY_PRINT);
	file_put_contents("streams.json", $jsondata);
    }


?>
