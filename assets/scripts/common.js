let config = {
    api: {
        startStream: 'http://crostr.tk/api/api.php?method=start&stream_name=',
        stopStream: 'http://crostr.tk/api/api.php?method=stop&stream_name=',
        webSocket: 'ws://streams.crostr.tk:8081/WebRTCApp',
        currentStream: 'https://crostr.tk/api/current_stream.json',
        streams: 'http://crostr.tk/api/streams.json',
        setCurrentStream: 'https://crostr.tk/api/api.php?method=setcurrentstream&stream_name='
    }
};

function waitForSocketReady(socket, callback) {
    setTimeout(function () {
        if (socket.readyState === 1) {
            callback();
        } else {
            waitForSocketReady(socket, callback);
        }
    }, 5);
}

function ajaxGet(url, successCallback) {
    let ajax = new XMLHttpRequest();

    ajax.open('GET', url, true);
    ajax.send();

    ajax.onreadystatechange = function() {
        if (ajax.readyState === 4 && ajax.status === 200) {
            successCallback(ajax.responseText);
        }
    }
}

function startAjaxPolling(url, interval, callback) {
    return setInterval(function() {
        ajaxGet(url, function(data) {
            callback(data);
        });
    }, interval);
}
