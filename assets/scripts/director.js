let myName = 'hidden_herbert_33';
let roomName = 'crostr';
let userType = 'director';

setTimeout(function() {
    register(myName, roomName);
}, 2000);

function setCurrentVideo(name) {
    let oldVideoElements = document.getElementsByClassName('live');
    for (let i = 0; i < oldVideoElements.length; i++) {
        oldVideoElements[i].classList.remove('live');
    }
    let videoElement = document.getElementById('video-' + name);
    if (videoElement) {
        videoElement.parentElement.classList.add('live');
    }
}

function playVideo(name) {
    setCurrentVideo(name);
    console.log('Play: ' + name);
    ajaxGet(
        config.api.setCurrentStream + name,
        function() {}
    );
}
