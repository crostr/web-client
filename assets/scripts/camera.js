let isStreaming = false,
    startBtn = document.getElementById('start-btn');

let roomName = 'crostr';
let myName = 'hugo_' + Math.floor(Math.random()*100);
let userType = 'camera';

startBtn.addEventListener('click', function() {
    if (!isStreaming) {
        isStreaming = true;
        register(myName, roomName);
        startBtn.innerHTML = 'Stop';
    } else {
        isStreaming = false;
        leaveRoom();
        startBtn.innerHTML = 'Start';
    }
});
