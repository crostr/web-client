let roomName = 'crostr';
let myName = 'hidden_heike_' + Math.floor(Math.random()*100);
let currentName = null;
let userType = 'player';

setTimeout(function() {
    register(myName, roomName);

    setTimeout(function() {
        startAjaxPolling(config.api.currentStream, 1000, function(data) {
            let json = JSON.parse(data);

            if (currentName !== json.current_stream) {
                currentName = json.current_stream;
                loadNewParticipant(currentName);
            }
        });
    }, 2000);
}, 2000);
